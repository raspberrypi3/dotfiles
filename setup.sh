#!/bin/bash

# Change directory to $HOME
cd $HOME

echo -e "\e[32mStarting setup process..."
echo "Updating repos and dependencies..."
echo -e "\e[39m"
sudo apt-get -y update
sudo apt-get -y upgrade

# General utilities
echo -e ""
echo -e "\e[32mInstalling general utilities..."
echo -e "\e[39m"
sudo apt-get -y install git tree
git config --global color.ui true

#Exa -  A modern replacement for ls
echo -e ""
echo -e "\e[32mInstalling Exa..."
echo -e "\e[39m"
export PATH=$PATH:$HOME/.cargo/bin
curl https://sh.rustup.rs > rust_install.sh
sh rust_install.sh -y
sudo apt-get -y install libgit2-dev cmake git libhttp-parser2.9 git make
git clone https://github.com/ogham/exa.git
cd exa
make install
sudo cp target/release/exa /usr/local/bin
sudo chmod 755 /usr/local/bin/exa

#Bat - A "cat" clone with wings
echo -e ""
echo -e "\e[32mInstalling Bat..."
echo -e "\e[39m"
cd $HOME
wget https://github.com/sharkdp/bat/releases/download/v0.15.4/bat_0.15.4_armhf.deb
sudo dpkg -i bat_0.15.4_armhf.deb

#Zsh
echo -e ""
echo -e "\e[32mInstalling Zsh..."
echo -e "\e[39m"
sudo apt-get -y install zsh
echo -e "\e[32mInstalling oh-my-zsh..."
echo -e "\e[39m"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
rm .zshrc
echo -e ""
echo -e "\e[32mDownloading .zshrc configuration file..."
echo -e "\e[39m"
wget https://gitlab.com/raspberrypi3/dotfiles/-/raw/master/.zshrc
sudo usermod --shell $(which zsh) $USER
zsh
source ~/.zshrc

# Post-installation tasks
# -- Remove useless files
echo -e "\e[32mRemoving temporal files..."
echo -e "\e[39m"
rm rust_install.sh
rm -rf exa/
rm bat_0.15.4_armhf.deb

echo -e "\e[32mInstallation finished!"
echo -e "Run 'zsh' to try it out."
echo -e "\e[32mEnjoy! :D"
echo -e "\e[39m"