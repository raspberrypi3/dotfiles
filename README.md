# dotfiles

Scripts de instalación y archivos de configuración tanto del sistema como de diferentes herramientas.

# Objetivo

Este repositorio tiene la finalidad de agilizar la instalación de herramientas y programas básicos de la Raspberry Pi con sistema operativo Raspbian, así como sincronizar los archivos de configuración de Bash y Zsh.

# Instalación

El fichero que realiza toda la instalación es **setup.sh**.

Para correr el script de instalación con sólo un comando, ejecutaremos el siguiente comando:
```zsh
$ wget -O - https://gitlab.com/raspberrypi3/dotfiles/-/raw/master/setup.sh | bash
```

El script instala diversos programas juntos a sus dependencias y descarga los archivos de configuración de Bash y Zsh. Ademásm, configura **Zsh** como shell por defecto.

Programas instalados:

* Git
* Zsh
* Oh-My-Zsh
* Exa
* Bat

# Referencias

* https://github.com/lucashour/dotfiles

# Contributing

1. Fork it!
2. Create your feature branch: git checkout -b my-new-feature
3. Commit your changes: git commit -am 'Add some feature'
4. Push to the branch: git push origin my-new-feature
5. Submit a pull request :D
